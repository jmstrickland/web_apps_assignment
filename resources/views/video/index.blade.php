@extends('layouts.app')
<script src="{{ asset('js/video.js') }}"></script>
@section('title', 'Main Videos Page')
@section('content')
    <div class="container">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('videos') }}
        @if(Auth::user() != null)
            <div class="btn-group m-1" role="group" aria-label="Basic example">
                <button class="btn btn-primary" onclick="window.location='{{ url("videos/create") }}'">New Video
                </button>
            </div>
        @endif
        <div class="row">
            @foreach ($videos as $video)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <iframe class="embed-responsive-item" src="{{ '/storage/' . $video->url }}"></iframe>
                        <div class="card-body">
                            <p class="card-text">{{ $video->title }}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" onclick="window.location='{{ url("videos/$video->id") }}'"
                                            class="btn btn-sm btn-outline-secondary">View <i class="fas fa-search"></i>
                                    </button>
                                </div>
                                <small class="text-muted">{{ 'Uploaded by ' }}<a
                                        href="{{ '/users/' . $video->user->id }}">{{ $video->user->name }}</a> {{  ' ' . \App\Helpers\TimeHelper::calculateTimeDifference($video->updated_at) . ' ago'  }}
                                </small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="d-flex justify-content-center">
            {{ $videos->links() }}
        </div>
    </div>
@endsection
