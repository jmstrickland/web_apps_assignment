<script src="{{ asset('js/video.js') }}"></script>
@extends('layouts.app')
@extends('tags.create_modal')
@section('title', 'Edit Current Video')
@section('content')
    <div class="container panel panel-default ">
        <h2 class="panel-heading">Edit Current Video</h2>
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('videos_edit', $video) }}
        <div id="errors_video_editing" class="alert alert-danger" hidden="true">
            <h4 class="alert-heading">There have been errors editing your video!</h4>
            <ul id="list_of_errors_video_editing">
            </ul>
        </div>
        <form id="edit_post_form">
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Enter Video Title" id="video_edit_title" value="{{ $video->title }}">
            </div>
            <div class="form-group">
                <input type="file" name="image" placeholder="Upload Video" id="video_edit_upload_video">
            </div>
            <div class="form-group">
                <select id="video_edit_tags_select" multiple="multiple" style="width: 75%"></select>
                <input type="button" class="btn btn-outline-success" id="edit_video_new_tag" value="New Tag"
                       data-toggle="modal" data-target="#modal_1"/>
            </div>
            <div class="form-group">
                <button class="btn btn-success" id="video_edit_submit">Submit <i class="fa fa-forward"></i> </button>
            </div>
        </form>
    </div>
    <input id="video_edit_id" style="display: none;" value="{{ $video->id }}"/>
@endsection
