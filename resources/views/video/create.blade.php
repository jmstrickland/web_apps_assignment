<script src="{{ asset('js/video.js') }}"></script>
@extends('layouts.app')
@extends('tags.create_modal')
@section('title', 'Create New Video')
@section('content')
    <div class="container panel panel-default ">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('videos_create') }}
        <h2 class="panel-heading">Create New Video</h2>
        <div class="alert alert-danger" hidden="true" id="errors_video_creation">
            <h4 class="alert-heading">There have been errors creating your video!</h4>
            <ul id="list_of_errors_video_creation">
            </ul>
        </div>
        <form id="contactForm">
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Enter Video Title" id="video_title">
            </div>
            <div class="form-group">
                <input type="file" name="image" placeholder="Upload Video" id="upload_video">
            </div>
            <div class="form-group">
                <select id="video_create_tags_select" multiple="multiple" style="width: 75%"></select>
                <input type="button" class="btn btn-outline-success" id="video_post_new_tag" value="New Tag"
                       data-toggle="modal" data-target="#modal_1"/>
            </div>
            <div class="form-group">
                <button class="btn btn-success" id="video_submit">Submit <i class="fa fa-forward"></i> </button>
            </div>
        </form>
    </div>
@endsection

