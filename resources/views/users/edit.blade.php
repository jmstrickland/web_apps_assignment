<script src="{{ asset('js/user.js') }}"></script>
@extends('layouts.app')
@section('title', 'Edit Current User')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="form-group">
                        <label for="user_edit_name" class="col-md-4 col-form-label">Name</label>
                        <input id="user_edit_name" value="{{ $user->name }}" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="user_edit_email" class="col-md-4 col-form-label">Email Address</label>
                        <input id="user_edit_email" value="{{ $user->email }}" type="text" class="form-control">
                    </div>
                    <div class="form-group">
                        <label for="user_edit_image" class="col-md-4 col-form-label">Profile Image</label>
                        <input id="user_edit_image" type="file" class="form-control">
                    </div>
                    <div>
                        <button type="submit" class="btn btn-primary m-1" id="user_edit_submit_button">Submit</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
