<script src="{{ asset('js/user.js') }}"></script>
@extends('layouts.app')
@section('title', 'Viewing ' . $user->name)
@section('content')
    <div class="container">
        <div class="row">
            @auth
                @if(\App\Helpers\AuthHelper::isUserOwnerOrAdmin($user))
                    <div class="col-12 m-2">
                        <div class="btn-group" role="group">
                            <button type="button" class="btn btn-secondary" onclick="window.location='{{ url("users/" . $user->id . "/edit") }}'"> Edit <i class="fa fa-edit"></i>  </button>
                            <button type="button" class="btn btn-secondary" id="user_show_delete_button">Delete <i class="fa fa-trash"></i> </button>
                        </div>
                    </div>
                @endif
            @endauth
            @if(is_null($user->image))
                <div class="col">
                    <h2 class="m-1">{{ $user->name }}</h2>
                    <h3 class="m-1">{{ $user->email }}</h3>
                </div>
            @else
                <div class="col">
                    <img src="{{ $user->image->url }}" class="img-fluid" alt="User's Profile Picture">
                </div>
                <div class="col">
                    <h2 class="m-1">{{ $user->name }}</h2>
                    <h3 class="m-1">{{ $user->email }}</h3>
                </div>
            @endif

        </div>
    </div>
@endsection
