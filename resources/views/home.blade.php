@extends('layouts.app')
@section('title', 'Home Dashboard')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <h3 class="card-header">{{ __('Dashboard') }}</h3>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                        {{ __('You are logged in!') }}
                    @endif
                </div>
                <div class="card">
                    <button class="btn btn-primary" onclick="window.location='/videos'">VIDEOS</button>
                    <button class="btn" onclick="window.location='/videos'"><i class="fas fa-video fa-10x"></i></button>

                </div>
                <div class="card">
                    <button class="btn btn-primary mb-2" onclick="window.location='/tags'">TAGS</button>
                    <button class="btn" onclick="window.location='/tags'"><i class="fas fa-tags fa-10x"></i></button>

                </div>
                <div class="card">
                    <button class="btn btn-primary" onclick="window.location='/posts'">POSTS</button>
                    <button class="btn" onclick="window.location='/posts'"><i class="fas fa-th fa-10x"></i></button>
                </div>
                <div class="card">
                    <h4 class="card-header">{{ __('Photo of the Day') }}</h4>
                    <img id="home_screen_photo_day_image" class="img-fluid mx-auto" src="{{ $wiki->photoUrl }}"
                         style="max-height: 500px;max-width: 500px; display: block;"
                         alt="{{ $wiki->photoTitle }}">
                    <a class="text-center" href="{{ $wiki->photoUrl }}"><small>{{ $wiki->photoTitle  }}</small></a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
