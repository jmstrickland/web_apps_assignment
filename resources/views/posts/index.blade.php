@extends('layouts.app')
@section('title', 'Main Posts Page')
@section('content')
    <div class="container">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('posts') }}
        @auth
            <div class="btn-group mb-1" role="group" aria-label="Basic example">
                <button class="btn btn-primary" onclick="window.location='{{ url("posts/create") }}'">New Post</button>
            </div>
        @endauth
        <div class="row">
            @foreach ($posts as $post)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        @if(!is_null($post->image))
                            <img class="card-img-top rounded mx-auto d-block mt-1" src="{{ $post->image->url ?? "" }}"
                                 style="height: 250px;width: 250px; display: block;" alt="{{ $post->image->url }}, uploaded on {{ $post->image->updated_at }}">
                        @else
                            <div class="rounded mx-auto d-block mt-1">
                                <i class="fas fa-blog fa-10x fa-fw" style="width: 250px; height: 250px"></i>
                            </div>
                        @endif
                        <div class="card-body">
                            <p class="card-text">{{ $post->title }}</p>
                            <div class="d-flex justify-content-between align-items-center">

                                <div class="btn-group">
                                    <button type="button" onclick="window.location='{{ url("posts/$post->id") }}'"
                                            class="btn btn-sm btn-outline-secondary">View <i class="fas fa-search"></i>
                                    </button>
                                </div>
                                {{--                                <small class="text-muted">{{ 'Uploaded by ' . $post->user->name . ' ' . \App\Helpers\TimeHelper::calculateTimeDifference($post->updated_at) . ' ago'}}</small>--}}
                                <small class="text-muted">{{ 'Uploaded by ' }}<a
                                        href="{{ '/users/' . $post->user->id }}">{{ $post->user->name }}</a> {{  ' ' . \App\Helpers\TimeHelper::calculateTimeDifference($post->updated_at) . ' ago'  }}
                                </small>

                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="d-flex justify-content-center">
            {{ $posts->links() }}
        </div>
    </div>
@endsection
