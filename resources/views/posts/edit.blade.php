<script src="{{ asset('js/post.js') }}"></script>
@extends('layouts.app')
@extends('tags.create_modal')
@section('title', 'Edit Current Post')
@section('content')
    <div class="container panel panel-default ">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('posts_edit', $post) }}
        <h2 class="panel-heading">Edit Current Post</h2>
        <div class="alert alert-danger" hidden="true" id="errors_post_editing">
            <h4 class="alert-heading">There have been errors editing your post!</h4>
            <ul id="list_of_errors_post_editing">
            </ul>
        </div>
        <form id="edit_post_form">
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Enter Post Title" id="post_edit_title" value="{{ $post->title }}">
            </div>
            <div class="form-group">
                <textarea class="form-control" id="post_edit_body" placeholder="Enter Post Body" rows="4">{{$post->body}}</textarea>
            </div>
            <div class="form-group">
                <input type="file" name="image" placeholder="Upload Image" id="post_edit_upload_image">
            </div>
            <div class="form-group">
                <select id="post_edit_tags_select" multiple="multiple" style="width: 75%"></select>
                <input type="button" class="btn btn-outline-success" id="edit_post_new_tag" value="New Tag"
                       data-toggle="modal" data-target="#modal_1"/>
            </div>
            <div class="form-group">
                <button class="btn btn-success" id="post_edit_submit">Submit <i class="fa fa-forward"></i> </button>
            </div>
        </form>
    </div>
    <input id="post_edit_id" style="display: none;" value="{{ $post->id }}"/>
    <input id="post_edit_image_id" style="display: none;" value="{{ $post->image ? $post->image->id : '' }}"/>
@endsection
