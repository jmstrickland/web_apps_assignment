<script src="{{ asset('js/post.js') }}"></script>
@extends('layouts.app')
@extends('tags.create_modal')
@section('title', 'Create New Post')
@section('content')
    <div class="container panel panel-default ">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('posts_create') }}
        <h2 class="panel-heading">Create New Post</h2>
        <div class="alert alert-danger" hidden="true" id="errors_post_creation">
            <h4 class="alert-heading">There have been errors creating your post!</h4>
            <ul id="list_of_errors_post_creation">
            </ul>
        </div>
        <form id="contactForm">
            <div class="form-group">
                <input type="text" name="title" class="form-control" placeholder="Enter Post Title" id="title">
            </div>
            <div class="form-group">
                <label for="body"></label>
                <textarea class="form-control" id="body" placeholder="Enter Post Body" rows="4"></textarea>
            </div>
            <div class="form-group">
                <input type="file" name="image" placeholder="Upload Image" id="upload_image">
            </div>
            <div class="form-group">
                <select id="post_create_tags_select" multiple="multiple" style="width: 75%"></select>
                <input type="button" class="btn btn-outline-success" id="create_post_new_tag" value="New Tag"
                       data-toggle="modal" data-target="#modal_1"/>
            </div>
            <div class="form-group">
                <button class="btn btn-success" id="create_post_submit">Submit <i class="fa fa-forward"></i></button>
            </div>
        </form>
    </div>
@endsection
