<script src="{{ asset('js/comment.js') }}"></script>
<script src="{{ asset('js/post.js') }}"></script>
@extends('layouts.app')
@section('title', 'Viewing ' . $post->title)
@section('content')
    <div class="container">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('posts_show', $post) }}
        @if(\App\Helpers\AuthHelper::isUserOwnerOrAdmin($post->user))
            <div class="btn-group" role="group">
                <button type="button" class="btn btn-secondary"
                        onclick="window.location='{{ url("posts/" . $post->id . "/edit") }}'"> Edit <i
                        class="fa fa-edit"></i></button>
                <button type="button" class="btn btn-secondary" id="post_show_delete_button">Delete <i
                        class="fa fa-trash"></i></button>
            </div>
            <input id="post_show_id" style="display: none;" value="{{ $post->id }}"/>
        @endif
        <div class="blog-post">
            <h2 class="blog-post-title">{{ $post->title }}</h2>
            <p class="blog-post-meta">{{ \App\Helpers\TimeHelper::calculateTimeDifference($post->updated_at) }} ago by
                <a href="{{ '/users/' . $post->user->id }}">{{ $post->user->name ?? 'Deleted' }}</a></p>
            @if(!is_null($post->image))
                <img src="{{ $post->image->url }}" style="max-height: 500px; max-width: 500px;">
            @endif
            <p>{{ $post->body  }}</p>
            <hr>
            @if($post->tags->count() != 0)
                <div>
                    <label>Tags</label>
                </div>

                @foreach($post->tags as $tag)
                    <button class="btn-primary"
                            onclick="window.location='{{ url("tags/" . $tag->id) }}'"> {{ $tag->name }} </button>
                @endforeach
            @endif
        </div>
        <div class="form-group" id="comment_area">
            @auth
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="post_show_comment_text">Comments</label>
                    </div>
                    <textarea class="form-control" id="post_show_comment_text" rows="3"></textarea>
                    <div class="form-group">
                        <button class="btn btn-success" id="post_show_comment_submit">Submit <i
                                class="fa fa-commenting-o"></i>
                        </button>
                    </div>
                    <div class="alert alert-danger" hidden="true" id="errors_comment_creation">
                        <ul id="list_of_errors_comment_creation">
                        </ul>
                    </div>
                </div>
            @endauth
            <div class="row">
                @foreach($comments as $comment)
                    <div class="col-md-4">
                        <div class="card mb-6 box-shadow m-1">
                            <div id="{{ $comment->id }}" class="card-body">
                                <p id="post_text_body">{{ $comment->text_body }}</p>
                            </div>
                            <div class="card-footer text-muted">
                                @if(!is_null($comment->user))
                                    <small>{{ \App\Helpers\TimeHelper::calculateTimeDifference($comment->updated_at) . ' ago by ' . $comment->user->name}}</small>
                                @else
                                    <small>By Deleted</small>
                                @endif
                                @if(\App\Helpers\AuthHelper::isUserOwnerOrAdmin($comment->user))
                                    <div class="btn-group">
                                        <button id="posts_show_comment_delete_{{ $comment->id }}"
                                                name="posts_show_comment_delete" type="button"
                                                class="btn-sm btn-danger float-right">Delete
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <button id="posts_show_comment_edit_{{ $comment->id }}"
                                                name="posts_show_comment_edit" type="button"
                                                class="btn-sm btn-primary float-right">Edit
                                            <i class="fa fa-edit"></i>
                                        </button>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div>
                {{ $comments->links() }}
            </div>
        </div>
    </div>

@endsection
