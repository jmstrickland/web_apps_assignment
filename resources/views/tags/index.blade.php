@extends('layouts.app')
@section('title', 'Main Tags Page')
@section('content')
    <div class="container">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('tags') }}
        <div class="row">
            @foreach ($tags as $tag)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <div class="card-body">
                            <p class="card-text">{{ $tag->name }}</p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" onclick="window.location='{{ url("tags/$tag->id") }}'"
                                            class="btn btn-sm btn-outline-secondary">View <i class="fas fa-search"></i>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>

    </div>

    <div class="d-flex justify-content-center">
        {{ $tags->links() }}
    </div>

@endsection
