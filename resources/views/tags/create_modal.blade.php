<script src="{{ asset('js/tag.js') }}"></script>
<div class="modal" tabindex="-1" role="dialog" id="modal_1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Create New Tag</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="text" id="modal_new_tag_input"/>
            </div>
            <div class="alert alert-danger" hidden="true" id="errors_tag_creation">
                <h4 class="alert-heading">There have been errors creating your tag!</h4>
                <ul id="list_of_errors_tag_creation">
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="modal_new_tag_submit">Save changes</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
