@extends('layouts.app')
@section('title', 'Viewing ' . $tag->name)
@section('content')
    <div class="container">
        {{ \Diglactic\Breadcrumbs\Breadcrumbs::render('tags_show', $tag) }}
        <div class="row">
            @foreach ($posts as $post)
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        @if(!is_null($post->image))
                            <img class="card-img-top rounded mx-auto d-block" src="{{ $post->image->url ?? "" }}" style="max-height: 250px;max-width: 250px; display: block;">
                        @endif
                        <div class="card-body">
                            <p class="card-text">{{ $post->title }}</p>
                            <div class="d-flex justify-content-between align-items-center">

                                <div class="btn-group">
                                    <button type="button" onclick="window.location='{{ url("posts/$post->id") }}'" class="btn btn-sm btn-outline-secondary">View</button>
                                </div>
                                <small class="text-muted">{{ 'Uploaded by ' . $post->user->name . ' ' . \App\Helpers\TimeHelper::calculateTimeDifference($post->updated_at) . ' ago'}}</small>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        {{  $posts->links() }}
    </div>

@endsection
