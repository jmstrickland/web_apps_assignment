window.addEventListener('load', function () {
    console.log("TAG JS")
    $('#modal_new_tag_submit').on('click', function (event){
        event.preventDefault();
        let tag_name = $('#modal_new_tag_input').val();
        $.ajax({
            url: '/tags/create',
            type: 'POST',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                name: tag_name
            },
            success: function (response) {
                console.log(response);
                $('#modal_1').modal('toggle');
                //window.location.reload(false);
            },
            error: function (failure) {
                console.log(failure);
                if (failure.status === 422) {
                    $('#list_of_errors_tag_creation').empty();
                    $('#errors_tag_creation').removeAttr('hidden');
                    $.each(failure.responseJSON.errors, function (key, value) {
                        $('#list_of_errors_tag_creation').append('<li>' + value + '</li');
                    });
                }
            }
        })
    })
});
