window.addEventListener('load', function () {

    $('#post_show_comment_submit').on('click', function (event) {
        event.preventDefault();
        let text = $('#post_show_comment_text').val();
        createComment(text);
    });

    $('#video_show_comment_submit').on('click', function (event) {
        event.preventDefault();
        let text = $('#video_show_comment_text').val();
        createComment(text);
    });



    function createComment(text) {
        let type = "";
        let id = window.location.href.split('/')[window.location.href.split('/').length - 1];
        if(window.location.href.includes("posts")){
            type = "App\\Models\\Post"
        }else{
            type = "App\\Models\\Video"
        }
        $.ajax({
            url: "/comments/create",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                text_body: text,
                commentable_type: type,
                commentable_id: id,
                user_id: $('meta[name="user-id"]').attr('content'),
            },
            success: function (response) {
                //console.log(response);
                window.location.reload(false);
            },
            error: function (response) {
                console.log(response);
                if (response.status === 422) { // when status code is 422, it's a validation issue
                    let listOfErrors = $('#list_of_errors_comment_creation');
                    listOfErrors.empty();
                    $('#errors_comment_creation').removeAttr('hidden');
                    $.each(response.responseJSON.errors, function (key, value) {
                        listOfErrors.append('<li>' + value + '</li');
                    });
                }
            }
        })
    }

    $('#post_show_comment_edit_submit').on('click', function (event) {
        event.preventDefault();
        let id = this.parent().id
        let text = $('#post_show_comment_text').val();
        console.log(id);

    });




    $('button[name = posts_show_comment_edit]').on('click', function (event) {
        event.preventDefault();
        let clickedBtnID = this.id;
        const commentID = clickedBtnID.split('_')[clickedBtnID.split('_').length - 1];
        let htmlToReplace = $('#' + commentID);

        let html = "<textarea class=\"form-control\" id=\"post_show_comment_text_edit_" + commentID + "\" rows=\"3\"" +
            " > "+ htmlToReplace.children()[0].innerHTML + " </textarea>";
        let htmlButton = "<button class=\"btn-sm btn-success\" id=\"post_show_comment_edit_submit\">Submit <i\n" +
            "                                class=\"fa fa-commenting-o\"></i>\n" +
            "                        </button>"

        htmlToReplace.empty();
        htmlToReplace.append(html);
        htmlToReplace.append(htmlButton);
        htmlToReplace.children()[1].addEventListener('click', function () {
            event.preventDefault();
            let text = $('#post_show_comment_text_edit_' + commentID).val();
            $.ajax({
                url: "/comments/" + commentID,
                type: "PUT",
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: {
                    text_body: text,
                },
                success: function (response) {
                    console.log(response);
                    window.location.reload(false);
                },
            })
        });
    });

    $('button[name = posts_show_comment_delete]').on('click', function (event) {
        event.preventDefault();
        let clickedBtnID = this.id;
        const commentID = clickedBtnID.split('_')[clickedBtnID.split('_').length - 1];
        Bootbox.confirm({
            message: "You have clicked to delete this post which is irreversible, are you sure?",
            buttons: {
                confirm: {
                    label: 'Yes',
                    className: 'btn-success'
                },
                cancel: {
                    label: 'No',
                    className: 'btn-danger'
                }
            },
            callback: function (result) {
                if (result) {
                    $.ajax({
                        url: '/comments/' + commentID,
                        type: 'DELETE',
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        success: function (response) {
                            window.location.reload(false);
                        }
                    })
                }
            }
        });
    });
});
