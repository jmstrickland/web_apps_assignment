window.addEventListener('load', function () {
    $('#user_edit_submit_button').on('click', function (event) {
        event.preventDefault();
        console.log("Clicked the user edit");
        let name  = $('#user_edit_name').val();
        let email  = $('#user_edit_email').val();
        let profileImage  = $('#user_edit_image')[0].files[0]
        let user_id = $('meta[name="user-id"]').attr('content');
        $.ajax({
            url: "/users/" + user_id,
            type: "PUT",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'name': name,
                'email': email
            },
            success: function (response) {
                console.log(response);

                if (profileImage != null) {
                    let type = 'App\\Models\\User';
                    let formData = new FormData();
                    formData.append('type', type);
                    formData.append('type_id', user_id);
                    // Attach file
                    formData.append('image', profileImage);
                    $.ajax({
                        url: "/images/create",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: formData,
                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                        processData: false, // NEEDED, DON'T OMIT THIS
                        success: function (image_response) {
                            console.log(image_response);
                            window.location.href = "/users/" + user_id;
                        },
                    });
                }
            }
        });
    });

    $('#user_show_delete_button').on('click', function (event) {
        event.preventDefault();
        console.log("Clicked Deltet USers");
        let user_id = $('meta[name="user-id"]').attr('content');
        $.ajax({
            url: "/users/" + user_id,
            type: "DELETE",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response);
            }
        });
    });
});
