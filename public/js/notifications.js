window.addEventListener('load', function () {

    let notificationDropdown = $('#navbar_dropdown_notifications');
    let notificationsToggle = notificationDropdown.find('a[data-toggle]');
    let notificationsCountElem = notificationsToggle.find('i[data-count]');
    let notificationChildren = notificationDropdown.children('a');
    let notificationCount = notificationDropdown.children('a').length;

    // if(notificationCount <= 0){
    //     notificationDropdown.hide();
    // }

    //console.log("NOTIFICATIONS");
    $.ajax({
        url: '/users/notifications',
        type: 'GET',
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
           // console.log(response);
            //console.log("response");
            //console.log(response);

            response.forEach(elem =>
                createNotification(elem, 'NOTIFICATION')
            );
            // response.each(notification =>
            //     console.log(notification));
        }
    })


    var pusher = new Pusher('800f141c66df0286c17c', {
        cluster: 'eu'
    });
    //Pusher.logToConsole = true;

    var channel = pusher.subscribe('my-channel');

    channel.bind('my-event' + $('meta[name="user-id"]').attr('content'), function (data) {
        // console.log("BIND");
        // console.log(data.user);
        // console.log(data.post);
        createNotification(data, 'BROADCAST');
        // console.log(data.user);
        // console.log(data.post);
    });

    $('')

    function decideTypeObjectBroadcast(element) {
        let type_object = "";
        if (element.hasOwnProperty('body')) {
            type_object = "post";
        } else {
            type_object = "video";
        }
        return type_object;
    }

    function decideTypeObjectNotification(element) {
        let type_object = "";
        if (element.includes('Post')) {
            type_object = 'post';
        } else {
            type_object = 'video';
        }
        return type_object
    }

    function createNotification(element, type) {
        let post_title = "";
        let post_id = "";
        let user_name = "";

        let type_object = "";
        let notification_id = "";
        if (type === 'BROADCAST') {
            let user = element.user;
            let post = element.post;
            post_title = post.title;
            post_id = post.id;
            user_name = user.name;
            type_object = decideTypeObjectBroadcast(element.post);
        } else {
            let data_array = element.data;
            notification_id = element.id;
            post_title = data_array.post_title;
            post_id = data_array.post_id;
            user_name = data_array.user_name;
            type_object = decideTypeObjectNotification(data_array.type_object);
        }
        let newNotificationHtml = '<div class="dropdown-item"> <a href="/' + type_object + 's/' + post_id + '"><b>'
            + user_name + '</b> has commented on your ' + type_object + ' <b>' + post_title + ' ' + '</b></a>' +
            '<i id="' + notification_id + '" class="fa fa-bell" aria-label="Press this to dismiss this notification"></i></div>'

        notificationDropdown.append(newNotificationHtml);

        const notificationBells = $('#navbar_dropdown_notifications').find("i");
        let currentBell = notificationBells[notificationBells.length - 1];

        currentBell.addEventListener('click', function () {
            readNotification(currentBell.id);
            $(currentBell).parent().remove();
        });
    }

    function readNotification(notificationId) {
        console.log(notificationId);
        $.ajax({
            url: '/users/notifications',
            type: 'PUT',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                'notification_id': notificationId
            },
            success: function (response) {
                console.log(response);
            }
        });
    }
});
