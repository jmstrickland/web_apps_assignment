window.addEventListener('load', function () {

    const type = "App\\Models\\Post"

    $('#post_create_tags_select').select2({
        placeholder: 'Select an option',
        ajax: {
            url: "/tags/multi",
            type: "GET",
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            processResults: function (data) {
                return {
                    results: data['results']
                };
            },
        }
    });

    $('#post_edit_tags_select').select2({
        placeholder: 'Select an option',
        ajax: {
            url: "/tags/multi",
            type: "GET",
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            processResults: function (data) {
                return {
                    results: data['results']
                };
            }
        }
    });

    var studentSelect = $('#post_edit_tags_select');
    $.ajax({
        url: "/posts/"+ + $('#post_edit_id').val() +"/get_tags" ,
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $.each(response, function (index, tag){
                var option = new Option(tag.name, tag.id, true, true);
                studentSelect.append(option).trigger('change');
            });
            studentSelect.trigger({
                type: 'select2:select',
                params: {
                    data: response
                }
            });
        }
    });

    function displayPostErrors(failure, listOfErrors, errorsParent) {
        //console.warn(failure.responseJSON.errors);
        //let listOfErrors = $('#list_of_errors_post_creation');
        listOfErrors.empty();
        //$('#errors_post_creation').removeAttr('hidden');
        errorsParent.removeAttr('hidden');

        $.each(failure.responseJSON.errors, function (key, value) {
            listOfErrors.append('<li>' + value + '</li');
        });
    }

    function createPost(title, body, listOfTagIds, type) {
        $.ajax({
            url: "/posts/create",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {
                title: title,
                body: body,
                user_id: $('meta[name="user-id"]').attr('content'),
                tags: listOfTagIds
            },
            success: function (response) {
                console.log(response);
                if ($('#upload_image')[0].files[0] != null) {
                    var formData = new FormData();
                    formData.append('type', type);
                    formData.append('type_id', response.id);
                    // Attach file
                    formData.append('image', $('#upload_image')[0].files[0]);
                    $.ajax({
                        url: "/images/create",
                        type: "POST",
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        },
                        data: formData,
                        contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                        processData: false, // NEEDED, DON'T OMIT THIS
                        success: function (image_response) {
                            console.log(image_response);
                            window.location.href = "/posts/" + response.id;
                        },
                    });
                } else {
                    window.location.href = "/posts/" + response.id;
                }
            },
            error: function (failure) {
                if (failure.status === 422) { // when status code is 422, it's a validation issue
                    displayPostErrors(failure,$('#list_of_errors_post_creation'), $('#errors_post_creation'));
                }
            }
        });
    }

    $('#create_post_submit').on('click', function (event) {
        event.preventDefault();

        let title = $('#title').val();
        let body = $('#body').val();
        let tags = $('#post_create_tags_select').select2('data');
        let listOfTagIds = [];
        tags.forEach(tag =>
            listOfTagIds.push(tag.id)
        )
        if ($('#upload_image')[0].files[0] == null) {
            Bootbox.confirm({
                message: "You haven't included a post picture, are you sure you wish to post?",
                buttons: {
                    confirm: {
                        label: 'Yes',
                        className: 'btn-success'
                    },
                    cancel: {
                        label: 'No',
                        className: 'btn-danger'
                    }
                },
                callback: function (result) {
                    console.log('This was logged in the callback: ' + result);
                    if (result) {
                        createPost(title, body, listOfTagIds, type);
                    }
                }
            });
        }
        else{
            createPost(title, body, listOfTagIds, type);
        }
    })

    $('#post_edit_submit').on('click', function (event) {
        event.preventDefault();
        let title = $('#post_edit_title').val();
        let body = $('#post_edit_body').val();
        let paramsHash = {};
        let id = $('#post_edit_id').val();
        let tags = $('#post_edit_tags_select').select2('data');
        let listOfTagIds = [];
        tags.forEach(tag =>
            listOfTagIds.push(tag.id)
        )
        console.log(title);
        console.log(body);
        console.log(id);
        if (title !== "") {
            paramsHash.title = title;
            console.log(title);
        }
        if (body !== "") {
            paramsHash.body = body;
            console.log(body);
        }
        if (listOfTagIds !== []) {
            paramsHash.tags = listOfTagIds;
            console.log(listOfTagIds);
        }

        console.log(paramsHash)
        $.ajax({
            url: '/posts/' + id,
            type: 'PUT',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: paramsHash,
            success: function (response) {
                console.log(response);
                if ($('#post_edit_upload_image')[0].files[0] != null) {
                    let imageID = $('#post_edit_image_id').val();
                    let formData = new FormData();
                    formData.append('image', $('#post_edit_upload_image')[0].files[0]);
                    if(imageID === "" ){
                        formData.append('type', type);
                        formData.append('type_id', response.id);
                        // Attach file
                        //formData.append('image', $('#upload_image')[0].files[0]);
                        $.ajax({
                            url: "/images/create",
                            type: "POST",
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: formData,
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false, // NEEDED, DON'T OMIT THIS
                            success: function (image_response) {
                                console.log(image_response);
                                window.location.href = "/posts/" + response.id;
                            },
                        });
                    }else{
                        $.ajax({
                            url: "/images/" + imageID,
                            type: 'POST',
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            cache: false,
                            data: formData,
                            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
                            processData: false, // NEEDED, DON'T OMIT THIS
                            success: function (imageResponse) {
                                console.log(imageResponse)
                                window.location.href = "/posts/" + id
                            },
                            error: function (errorResponse) {
                                console.log(errorResponse);
                                if (errorResponse.status === 422) { // when status code is 422, it's a validation issue
                                    displayPostErrors(errorResponse,$('#list_of_errors_post_editing'), $('#errors_post_editing'));
                                }
                            }
                        });
                    }

                } else {
                    console.log("Log is undefined");
                    window.location.href = "/posts/" + id
                }
            }
        });
    });

    $('#post_show_delete_button').on('click', function (event) {
        event.preventDefault();
        let post_id = $('#post_show_id').val();
        $.ajax({
            url: "/posts/" + post_id,
            type: 'DELETE',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response);
                window.location.href = "/posts/";
            },
            error: function (response) {
                console.log(response);
            }
        });

    });
});
