window.addEventListener('load', function () {

    $('#video_create_tags_select').select2({
        placeholder: 'Select an option',
        ajax: {
            url: "/tags/multi",
            type: "GET",
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            processResults: function (data) {
                return {
                    results: data['results']
                };
            },
        }
    });

    $('#video_edit_tags_select').select2({
        placeholder: 'Select an option',
        ajax: {
            url: "/tags/multi",
            type: "GET",
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: {},
            processResults: function (data) {
                return {
                    results: data['results']
                };
            },
        }
    });


    var studentSelect = $('#video_edit_tags_select');
    $.ajax({
        url: "/videos/" + $('#video_edit_id').val() + "/get_tags" ,
        type: "GET",
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        success: function (response) {
            $.each(response, function (index, tag){
                var option = new Option(tag.name, tag.id, true, true);
                studentSelect.append(option).trigger('change');
            });
            studentSelect.trigger({
                type: 'select2:select',
                params: {
                    data: response
                }
            });
        }
    });

    $('#video_submit').on('click', function (event) {
        event.preventDefault();

        let title = $('#video_title').val();
        var formData = new FormData();
        formData.append('title', title);
        formData.append('user_id', $('meta[name="user-id"]').attr('content'));
        // Attach file
        formData.append('video', $('#upload_video')[0].files[0]);
        let tags = $('#video_create_tags_select').select2('data');
        let listOfTagIds = [];
        tags.forEach(tag =>
            listOfTagIds.push(tag.id)
        )
        console.log(listOfTagIds);
        formData.append('tags', JSON.stringify(listOfTagIds));
        $.ajax({
            url: "/videos/create",
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: function (video_response) {
                console.log(video_response);
                window.location.href = "/videos/" + video_response.id;
            },
            error: function (failure) {
                if (failure.status === 422) { // when status code is 422, it's a validation issue
                    displayPostErrors(failure, $('#list_of_errors_video_creation'), $('#errors_video_creation'))
                    // let listOfErrors = $('#list_of_errors_video_creation');
                    // listOfErrors.empty();
                    // $('#errors_video_creation').removeAttr('hidden');
                    // $.each(failure.responseJSON.errors, function (key, value) {
                    //     listOfErrors.append('<li>' + value + '</li');
                    // });
                }
            }
        });
    });

    $('#video_show_delete_button').on('click', function (event) {
        event.preventDefault();
        let video_id = $('#video_show_id').val();
        console.log(video_id);
        $.ajax({
            url: "/videos/" + video_id,
            type: "DELETE",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            success: function (response) {
                console.log(response)
                window.location.href = "/videos";
            },
        });


    });

    $('#video_edit_submit').on('click', function (event) {
        event.preventDefault();
        let title = $('#video_edit_title').val();
        var formData = new FormData();
        formData.append('title', title);
        if($('#video_edit_upload_video')[0].files.length !== 0){
            console.log("HAHAHAHAH");
            formData.append('video', $('#video_edit_upload_video')[0].files[0]);
        }
        let tags = $('#video_edit_tags_select').select2('data');
        //if (listOfTagIds !== []) {
            if(tags !== undefined){
                let listOfTagIds = [];
                tags.forEach(tag =>
                    listOfTagIds.push(tag.id)
                )
                formData.append('tags', JSON.stringify(listOfTagIds));
                console.log(listOfTagIds);
            }

       // }
        console.log(formData);
        $.ajax({
            url: "/videos/" + $('#video_edit_id').val(),
            type: "POST",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            data: formData,
            contentType: false, // NEEDED, DON'T OMIT THIS (requires jQuery 1.6+)
            processData: false, // NEEDED, DON'T OMIT THIS
            success: function (video_response) {
                console.log(video_response);
                window.location.href = "/videos/" + video_response.id;
            },
            error: function (failure) {
                console.log(failure);
                if (failure.status === 422) {
                    displayPostErrors(failure, $('#list_of_errors_video_editing'), $('#errors_video_editing'));
                }
            }
        });
    });

    function displayPostErrors(failure, listOfErrors, errorsParent) {
        //console.warn(failure.responseJSON.errors);
        //let listOfErrors = $('#list_of_errors_post_creation');
        listOfErrors.empty();
        //$('#errors_post_creation').removeAttr('hidden');
        errorsParent.removeAttr('hidden');

        $.each(failure.responseJSON.errors, function (key, value) {
            listOfErrors.append('<li>' + value + '</li');
        });
    }
});
