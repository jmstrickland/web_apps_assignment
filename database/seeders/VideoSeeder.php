<?php


namespace Database\Seeders;


use App\Models\User;
use Illuminate\Database\Seeder;

class VideoSeeder extends Seeder
{
    public function run()
    {
        User::factory(10)->hasVideos(3)->create();
    }
}
