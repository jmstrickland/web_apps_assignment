<?php


namespace Database\Factories;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class ImageFactory extends Factory
{
    public function definition()
    {
        $imageables = [
            User::class,
            Post::class
        ];
        $imageableType = $this->faker->randomElement($imageables);
        $imageable = $imageableType::factory()->create();
        return [
//            'title' => $this->faker->jobTitle,
            'url' => $this->faker->imageUrl(),
            'imageable_type' => $imageableType,
            'imageable_id' => $imageable->id,
        ];
    }
}
