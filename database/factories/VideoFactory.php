<?php


namespace Database\Factories;


use Illuminate\Database\Eloquent\Factories\Factory;

class VideoFactory extends Factory
{
    public function definition()
    {
        return [
            'title' => $this->faker->jobTitle,
            'url' => $this->faker->imageUrl(),
        ];
    }
}
