<?php

namespace Database\Factories;

use App\Models\Comment;
use App\Models\Post;
use App\Models\Video;
use Illuminate\Database\Eloquent\Factories\Factory;

class CommentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $commentables = [
            Video::class,
            Post::class
        ];
        $commentableType = $this->faker->randomElement($commentables);
        $commentable = $commentableType::factory()->create();
        return [
            'text_body' => $this->faker->paragraph,
            'commentable_type' => $commentableType,
            'commentable_id' => $commentable->id,
        ];
    }
}
