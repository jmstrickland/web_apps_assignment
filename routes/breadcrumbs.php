<?php

use Diglactic\Breadcrumbs\Breadcrumbs;

Breadcrumbs::for('posts', function ($trail) {
    $trail->push("Posts", url('posts'));
});

Breadcrumbs::for('posts_show', function ($trail, $post) {
    $trail->parent('posts');
    $trail->push($post->title, url('posts', $post->id));
});

Breadcrumbs::for('posts_edit', function ($trail, $post) {
    $trail->parent('posts_show', $post);
    $trail->push('Edit', url('posts', [$post->id, '/edit']));
});

Breadcrumbs::for('posts_create', function ($trail) {
    $trail->parent('posts');
    $trail->push('Create', url('posts/create'));
});

Breadcrumbs::for('tags', function ($trail) {
    $trail->push("Tags", url('tags'));
});

Breadcrumbs::for('tags_show', function ($trail, $tag) {
    $trail->parent('tags');
    $trail->push($tag->name, url('tags/', $tag->id));
});

Breadcrumbs::for('videos', function ($trail) {
    $trail->push("Videos", url('videos'));
});

Breadcrumbs::for('videos_create', function ($trail) {
    $trail->parent('videos');
    $trail->push("Create", url('videos/create'));
});

Breadcrumbs::for('videos_show', function ($trail, $video) {
    $trail->parent('videos');
    $trail->push($video->title, url('videos', $video->id));
});

Breadcrumbs::for('videos_edit', function ($trail, $video) {
    $trail->parent('videos_show', $video);
    $trail->push('Edit', url('videos', [$video->id, '/edit']));
});
