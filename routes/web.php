<?php

use App\Models\Post;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//dd(app());
Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

//Route::get('/', function () {
//
//    return view('home', );
//});
Route::get('/', 'App\Http\Controllers\HomeController@index');

Route::get('/videos', 'App\Http\Controllers\VideoController@index');
Route::get('/videos/create', 'App\Http\Controllers\VideoController@create');
Route::get('/videos/{id}/edit', 'App\Http\Controllers\VideoController@edit');
Route::get('/videos/{id}', 'App\Http\Controllers\VideoController@show');
Route::post('/videos/create', 'App\Http\Controllers\VideoController@store');
Route::delete('/videos/{id}', 'App\Http\Controllers\VideoController@delete');
Route::post('videos/{id}', 'App\Http\Controllers\VideoController@update');
Route::get('/videos/{id}/get_tags', 'App\Http\Controllers\VideoController@tags');

Route::get('/posts', function () {
    return view('posts.index', ['posts' => Post::paginate(12)]);
});
Route::get('/posts/create', 'App\Http\Controllers\PostController@create');
Route::get('/posts/{id}/edit', 'App\Http\Controllers\PostController@edit');
Route::put('/posts/{id}', 'App\Http\Controllers\PostController@update');
Route::get('/posts/{id}', 'App\Http\Controllers\PostController@show');
Route::delete('/posts/{id}', 'App\Http\Controllers\PostController@delete');
Route::get('/posts/{id}/get_tags', 'App\Http\Controllers\PostController@tags');
Route::post('/posts/create', 'App\Http\Controllers\PostController@store');

Route::post('images/create', 'App\Http\Controllers\ImageController@store');
Route::post('/images/{id}', 'App\Http\Controllers\ImageController@update');

Route::post('/comments/create', 'App\Http\Controllers\CommentController@store');
Route::delete('/comments/{id}', 'App\Http\Controllers\CommentController@delete');
Route::put('/comments/{id}', 'App\Http\Controllers\CommentController@update');

Route::get('/tags/multi', 'App\Http\Controllers\TagController@multi_select_tags');
Route::post('/tags/create', 'App\Http\Controllers\TagController@store');
Route::get('/tags/{id}', 'App\Http\Controllers\TagController@show');
Route::get('/tags', 'App\Http\Controllers\TagController@index');

Route::get('/403', function (){
    return view('errors.403');
});

Route::get('/users/example', [App\Http\Controllers\UserController::class, 'example']);

Route::get('/users/notifications','App\Http\Controllers\UserController@notifications');
Route::put('/users/notifications','App\Http\Controllers\UserController@read_notification');
Route::get('/users/{id}', 'App\Http\Controllers\UserController@show');
Route::get('/users/{id}/edit', 'App\Http\Controllers\UserController@edit');
Route::put('/users/{id}', 'App\Http\Controllers\UserController@update');
Route::delete('/users/{id}', 'App\Http\Controllers\UserController@delete');
