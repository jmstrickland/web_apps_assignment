<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    use HasFactory;

    /**
     * Get the parent commentable model (post or video).
     */
    public function commentable()
    {
        return $this->morphTo();
    }

    /**
     * Get the user associated with this comment.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

//    /**
//     * Get the post associated with this comment.
//     */
//    public function post()
//    {
//        return $this->belongsTo('App\Models\Post');
//    }

}
