<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'body',
    ];

    /**
     * Get the user associated with this post.
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User');
    }

    /**
     * The comments that belong to the post.
     */
    public function comments()
    {
        return $this->morphMany(Comment::class, 'commentable');
    }

    public  function image()
    {
        return $this->morphOne(Image::class, 'imageable');
    }

    /**
     * Get all of the tags for the post.
     */
    public function tags()
    {
        return $this->morphToMany(Tag::class, 'taggable');
    }

}
