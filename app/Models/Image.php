<?php
Namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    use HasFactory;

    /**
     * Get the user associated with this post.
     */
    public function imageable()
    {
        return $this->morphTo();
    }

}
