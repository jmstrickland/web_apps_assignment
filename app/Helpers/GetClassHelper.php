<?php


namespace App\Helpers;


use App\Models\Comment;
use App\Models\Post;
use App\Models\User;
use App\Models\Video;

class GetClassHelper
{
    static function getOriginalClassFromType($type): string
    {
        if(str_contains($type, 'post') ){
            return (Post::class);
        }elseif (str_contains($type, 'video')){
            return (Video::class);
        }elseif (str_contains($type, 'comment')){
            return (Comment::class);
        }elseif (str_contains($type, 'user')){
            return (User::class);
        }
    }
}
