<?php


namespace App\Helpers;

use App\Models\User;
use Auth;

class AuthHelper
{
    static function isUserOwnerOrAdmin(User $original_user): bool
    {
        return Auth::check() && (Auth::user()->id == $original_user->id || Auth::user()->admin);
    }
}
