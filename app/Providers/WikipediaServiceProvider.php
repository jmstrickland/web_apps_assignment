<?php


namespace App\Providers;


use App\Wikipedia;
use Illuminate\Support\ServiceProvider;

class WikipediaServiceProvider extends ServiceProvider
{

    public function register()
    {
        app()->singleton('wiki', function ($app) {
            return new Wikipedia();
        });
    }
}
