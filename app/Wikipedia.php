<?php


namespace App;


use Carbon\Carbon;
use Illuminate\Support\Facades\Http;

class Wikipedia
{

    public $photoUrl;
    public $photoTitle;

    public function __construct(){
        $array_of_elements = $this->getUrlAndTitle();
        $this->photoUrl = $array_of_elements[0];
        $this->photoTitle = $array_of_elements[1];
    }

    public function getUrlAndTitle()
    {
        $time = Carbon::now();
        $time = $time->toDateString();

        $title = "Template:POTD_protected/" . $time;

        $params = [
            "action" => "query",
            "format" => "json",
            "formatversion" => "2",
            "prop" => "images",
            "titles" => $title
        ];

        $response = Http::get("https://en.wikipedia.org/w/api.php", $params);
        $filename = $response->json()["query"]["pages"][0]["images"][0]["title"];

        $url = $this->fetchImageSrc($filename);
        return [$url, $filename];
    }

    private function fetchImageSrc($filename)
    {
        $params = [
            "action" => "query",
            "format" => "json",
            "prop" => "imageinfo",
            "iiprop" => "url",
            "titles" => $filename
        ];
        $response = Http::get("https://en.wikipedia.org/w/api.php", $params);
        return array_pop($response->json()['query']['pages'])['imageinfo']['0']['url'];
    }
}
