<?php

namespace App\Http\Middleware;

use App\Helpers\AuthHelper;
use App\Helpers\GetClassHelper;
use App\Models\User;
use Closure;
use Illuminate\Http\Request;

class EnsureUserIsAdminOrOwner
{
    /**
     * Handle an incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $original_user_id = GetClassHelper::
        getOriginalClassFromType($request->getRequestUri())::findOrFail($request->id)->user_id;
        if (AuthHelper::isUserOwnerOrAdmin(User::findOrFail($original_user_id))) {
            return $next($request);
        } else {
            return redirect('/403');
        }
    }
}
