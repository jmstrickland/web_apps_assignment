<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('owner')
            ->only(['edit', 'update', 'delete']);
    }

    public function index()
    {
        return view('index', ['users' => User::all()]);
    }

    public function show($id)
    {
        return view('users.show', ['user' => User::findOrFail($id)]);
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);
        return view('users.edit', [
            'user' => $user
        ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required_without_all:video',
            'email' => 'required_without_all:title'
        ]);
        $user = User::findOrFail($id);
        $request->input('name') ? $user->name = $request->name : null;
        $request->input('email') ? $user->email = $request->email : null;
        $user->save();
    }

    public function delete($id)
    {
        $user = User::findOrFail($id);
        $user->delete();
    }

    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(5)->get()->toArray();
    }

    public function read_notification(Request $request)
    {
        foreach (auth()->user()->unreadNotifications as $notification) {
            echo $notification->type;
            if ($notification->id == $request->notification_id) {
                $notification->markAsRead();
            }
        }
    }
}
