<?php


namespace App\Http\Controllers;


use App\Models\Tag;
use App\Models\Video;
use FFMpeg\FFMpeg;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

//use Pbmedia\LaravelFFMpeg\FFMpeg;

class VideoController extends Controller
{

    public function __construct()
    {
        $this->middleware('owner')
            ->only(['edit', 'update', 'delete']);
    }

    public function index()
    {
        return view('video.index', ['videos' => Video::paginate(21)]);
    }

    public function create()
    {
        if (Auth::check()) {
            return view('video.create');
        } else {
            return redirect('/403');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'user_id' => 'required',
            'video' => 'required|mimes:flv,mp4,avi,wmv'
        ]);
        $video = $request->video;
        $file_url = Storage::putFile('videos', $video->path());

        $video = new Video;
        $video->title = $request->title;
        $video->user_id = $request->user_id;
        $video->url = $file_url;
        $video->save();
        if($request->input('tags')){
            $array_of_tags = json_decode($request->tags);
            $list_of_tags = [];
            foreach ($array_of_tags as $tag) {
                $found_tag = Tag::findOrFail($tag);
                array_push($list_of_tags, $found_tag);
            }
            $video->tags()->saveMany($list_of_tags);
        }
        return response()->json($video);

    }

    public function show($id)
    {
        $video = Video::findOrFail($id);
        return view('video.show', [
            'video' => $video,
            'comments' => $video->comments()->orderByDesc('id')->paginate(10)
        ]);
    }

    public function edit($id)
    {
        $video = Video::findOrFail($id);
        return view('video.edit', [
            'video' => $video
        ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required_without_all:video',
            'video' => 'sometimes|required_without_all:title|mimes:flv,mp4,avi,wmv'
        ]);
        $video = Video::findOrFail($id);
        $request->input('title') ? $video->title = $request->title : null;
        if($request->has('video')){
            Storage::delete($video->url);
            $file_url = Storage::putFile('videos', $request->video->path());
            $video->url = $file_url;
        }
        $video->tags()->detach($video->tags()->allRelatedIds());
        Log::debug('HELLO TAGS');
        Log::debug($request->input('tags'));
        if($request->input('tags')){
            $array_of_tags = json_decode($request->tags);
            // $list_of_tags = [];
            foreach ($array_of_tags as $tag) {
                $found_tag = Tag::findOrFail($tag);
                // array_push($list_of_tags, $found_tag);
                $video->tags()->save($found_tag);
            }
            //$post->tags()->saveMany($list_of_tags);
        }
        $video->save();
        return response()->json($video);
    }

    public function delete($id)
    {
        $video = Video::findOrFail($id);
        Storage::delete($video->url);
        $video->delete();
    }

    public function tags($id)
    {
        $video = Video::findOrFail($id);
        return $video->tags;
    }

}
