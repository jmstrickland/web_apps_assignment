<?php


namespace App\Http\Controllers;


use App\Events\PostCommentedOn;
use App\Models\Comment;
use App\Models\Post;
use App\Models\Video;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Notification;

class CommentController extends Controller
{
    public function __construct()
    {
        $this->middleware('owner')
            ->only(['delete','update']);
    }

    public function store(Request $request) {

        $request->validate([
            'commentable_type' => 'required',
            'commentable_id' => 'required',
            'text_body' => 'required',
            'user_id' => 'required',
        ]);

        $type = $request->commentable_type;
        $type_id = $request->commentable_id;

        $comment = new Comment();
        $comment->text_body = $request->text_body;
        $comment->user_id = $request->user_id;
        $comment->commentable_type = $type;
        $comment->commentable_id = $type_id;
        $comment->save();

        if(str_contains($type, 'Post') ){
            $object = Post::findOrFail($type_id);
        }
        else{
            $object = Video::findOrFail($type_id);
        }
        if($object->user_id != Auth::user()->id){
            broadcast(new PostCommentedOn(Auth::user(), $object));
            Notification::send($comment->commentable->user, new PostCommentedOn(Auth::user(), $object));
        }

        return response()->json($comment);
    }

    public function delete($id){
        $comment = Comment::findOrFail($id);
        $comment->delete();
    }

    public function update($id, Request $request){
        $comment = Comment::findOrFail($id);
        $comment->text_body = $request->text_body;
        $comment->save();
    }

}
