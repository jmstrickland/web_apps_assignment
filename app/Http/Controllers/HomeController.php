<?php

namespace App\Http\Controllers;

use App\Wikipedia;
use Illuminate\Contracts\Support\Renderable;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @param Wikipedia $wiki
     * @return Renderable
     */
    public function index(Wikipedia $wiki)
    {
        return view('home', ['wiki' => $wiki]);
    }
}
