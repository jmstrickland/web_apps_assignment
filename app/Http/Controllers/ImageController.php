<?php


namespace App\Http\Controllers;


use App\Models\Image;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\ImageManagerStatic as Intervention;

//use Intervention\Image\ImageManagerStatic;

class ImageController extends Controller
{
    public function store(Request $request)
    {
        $filename = $this->save_image($request);
        $image = new Image;
        $image->url = '/storage/images/' . $filename;
        $image->imageable_type = $request->type;
        $image->imageable_id = $request->type_id;
        $image->save();

        return Response()->json([
            "image" => $image
        ]);
    }

    public function update($id, Request $request)
    {
        $filename = $this->save_image($request);
        $image = Image::findOrFail($id);
        if (file_exists($image->url)){
            Storage::delete($image->url);
        }
        $image->url = '/storage/images/' . $filename;
        $image->save();
    }

    private function save_image(Request $request): string
    {
        Log::debug($request->image);
        $request->validate([
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg,jpg'
        ]);
        $image = $request->image;
        $filename = time() . '.' . $image->extension();
        $image_resize = Intervention::make($image->getRealPath());
//        $image_resize->resize(250, 250);
        $image_resize->save(storage_path('app\\public\\images\\' . $filename));
        return $filename;
    }

}
