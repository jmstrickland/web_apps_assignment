<?php


namespace App\Http\Controllers;


use App\Models\Post;
use App\Models\Tag;
use Auth;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('owner')
            ->only(['edit', 'update', 'delete']);
    }

    public function create()
    {
        if (Auth::check()) {
            return view('/posts/create', ['tags' => Tag::all()]);
        } else {
            return redirect('/403');
        }
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'body' => 'required',
            'user_id' => 'required'
        ]);
        $post = new Post;
        $post->title = $request->title;
        $post->user_id = $request->user_id;
        $post->body = $request->body;
        $post->save();
        if($request->input('tags')){
            $array_of_tags = $request->tags;
            $list_of_tags = [];
            foreach ($array_of_tags as $tag) {
                $found_tag = Tag::findOrFail($tag);
                array_push($list_of_tags, $found_tag);
            }
            $post->tags()->saveMany($list_of_tags);
        }
        return response()->json($post);

    }

    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.show', [
            'post' => $post,
            'comments' => $post->comments()->orderByDesc('id')->paginate(10)
        ]);

    }

    public function edit($id)
    {
        $post = Post::findOrFail($id);
        return view('posts.edit', [
            'post' => $post
        ]);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required_without_all:video',
            'body' => 'required_without_all:title'
        ]);
        $post = Post::findOrFail($id);
        $request->input('title') ? $post->title = $request->title : null;
        $request->input('body') ? $post->body = $request->body : null;
        $post->tags()->detach($post->tags()->allRelatedIds());
        if($request->input('tags')){
            $array_of_tags = $request->tags;
           // $list_of_tags = [];
            foreach ($array_of_tags as $tag) {
                $found_tag = Tag::findOrFail($tag);
               // array_push($list_of_tags, $found_tag);
                $post->tags()->save($found_tag);
            }
            //$post->tags()->saveMany($list_of_tags);
        }
        //$post->update($request->all());
        $post->save();
        return response()->json($post);
    }

    public function delete($id)
    {
        $post = Post::findOrFail($id);
        $image = $post->image();
        if ($image != null) {
            $image->delete();
        }
        $post->delete();
    }

    public function tags($id)
    {
        $post = Post::findOrFail($id);
        return $post->tags;
    }


}
