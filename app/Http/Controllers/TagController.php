<?php


namespace App\Http\Controllers;


use App\Models\Tag;
use Illuminate\Http\Request;

class TagController
{
    public function store(Request $request){
        $request->validate([
            'name' => 'required|unique:App\Models\Tag,name',
        ]);
        $tag = new Tag();
        $tag->name = $request->name;
        $tag->save();
        return response()->json($tag);
    }

    public function show($id){
        $tag = Tag::findOrFail($id);
        return view('tags.show', ['tag' => $tag , 'posts' => $tag->posts()->paginate(21), 'videos' => $tag->videos()->paginate(21)]);
    }

    public function index(){
        $tags = Tag::paginate(21);
        return view('tags.index', ['tags' => $tags]);
    }

    public function multi_select_tags(){
        $array_of_tags = array();
        foreach (Tag::all() as $tag) {
            array_push($array_of_tags, array('id' => $tag->id, 'text' => $tag->name));
        }
        $correct_array = array('results' => $array_of_tags);
        return json_encode($correct_array);
    }
}
