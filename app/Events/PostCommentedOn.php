<?php


namespace App\Events;


use App\Models\User;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Queue\SerializesModels;

class PostCommentedOn extends Notification implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels, Queueable;

    //public $message;
    public $user;
    public $post;

    public function __construct(User $user, $post)
    {
        $this->user = $user;
        $this->post = $post;
    }

    public function broadcastOn()
    {
        return ['my-channel'];
    }
//
    public function broadcastAs()
    {
        return 'my-event' . $this->post->user->id;
    }

    public function via($notifiable)
    {
        return ['database'];
    }

    public function toMail($notifiable){
        return (new MailMessage())
        ->line('Introduction')
            ->action('Action', url('/'))
            ->line('End');
    }

    public function toDatabase($notifiable)
    {
        return [
            'user_name' => $this->user->name,
            'post_id' => $this->post->id,
            'post_title' => $this->post->title,
            'type_object' => class_basename($this->post),
            # TODO Will need to change all this post stuff to ambiguous stuff
        ];
    }
}
